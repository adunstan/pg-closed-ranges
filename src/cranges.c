
#include "postgres.h"
#include "fmgr.h"
#include "utils/builtins.h"
#include "utils/date.h"
#include "utils/rangetypes.h"

#if PG_VERSION_NUM >= 110000
#define PG_GETARG_RANGE(x) PG_GETARG_RANGE_P((x))
#define PG_RETURN_RANGE(x) PG_RETURN_RANGE_P((x))
#endif

PG_MODULE_MAGIC;

/*
 *----------------------------------------------------------
 * CANONICAL FUNCTIONS
 *
 *	 Functions for specific built-in range types.
 *----------------------------------------------------------
 */

extern Datum cint4range_canonical(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(cint4range_canonical);

Datum
cint4range_canonical(PG_FUNCTION_ARGS)
{
	RangeType  *r = PG_GETARG_RANGE(0);
	TypeCacheEntry *typcache;
	RangeBound	lower;
	RangeBound	upper;
	bool		empty;

	typcache = range_get_typcache(fcinfo, RangeTypeGetOid(r));

	range_deserialize(typcache, r, &lower, &upper, &empty);

	if (empty)
		PG_RETURN_RANGE(r);

	if (!lower.infinite && !lower.inclusive)
	{
		lower.val = DirectFunctionCall2(int4pl, lower.val, Int32GetDatum(1));
		lower.inclusive = true;
	}

	if (!upper.infinite && !upper.inclusive)
	{
		upper.val = DirectFunctionCall2(int4pl, upper.val, Int32GetDatum(-1));
		upper.inclusive = true;
	}

	PG_RETURN_RANGE(range_serialize(typcache, &lower, &upper, false));
}

extern Datum cint8range_canonical(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(cint8range_canonical);

Datum
cint8range_canonical(PG_FUNCTION_ARGS)
{
	RangeType  *r = PG_GETARG_RANGE(0);
	TypeCacheEntry *typcache;
	RangeBound	lower;
	RangeBound	upper;
	bool		empty;

	typcache = range_get_typcache(fcinfo, RangeTypeGetOid(r));

	range_deserialize(typcache, r, &lower, &upper, &empty);

	if (empty)
		PG_RETURN_RANGE(r);

	if (!lower.infinite && !lower.inclusive)
	{
		lower.val = DirectFunctionCall2(int8pl, lower.val, Int64GetDatum(1));
		lower.inclusive = true;
	}

	if (!upper.infinite && !upper.inclusive)
	{
		upper.val = DirectFunctionCall2(int8pl, upper.val, Int64GetDatum(-1));
		upper.inclusive = true;
	}

	PG_RETURN_RANGE(range_serialize(typcache, &lower, &upper, false));
}

extern Datum cdaterange_canonical(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(cdaterange_canonical);

Datum
cdaterange_canonical(PG_FUNCTION_ARGS)
{
	RangeType  *r = PG_GETARG_RANGE(0);
	TypeCacheEntry *typcache;
	RangeBound	lower;
	RangeBound	upper;
	bool		empty;

	typcache = range_get_typcache(fcinfo, RangeTypeGetOid(r));

	range_deserialize(typcache, r, &lower, &upper, &empty);

	if (empty)
		PG_RETURN_RANGE(r);

	if (!lower.infinite && !lower.inclusive)
	{
		lower.val = DirectFunctionCall2(date_pli, lower.val, Int32GetDatum(1));
		lower.inclusive = true;
	}

	if (!upper.infinite && !upper.inclusive)
	{
		upper.val = DirectFunctionCall2(date_pli, upper.val, Int32GetDatum(-1));
		upper.inclusive = true;
	}

	PG_RETURN_RANGE(range_serialize(typcache, &lower, &upper, false));
}


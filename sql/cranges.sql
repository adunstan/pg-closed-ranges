-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION cranges" to load this file. \quit

CREATE TYPE cdaterange;

CREATE FUNCTION cdaterange_canonical(cdaterange) 
RETURNS cdaterange
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT IMMUTABLE;

CREATE TYPE cdaterange as RANGE (
       SUBTYPE = date,
       CANONICAL = cdaterange_canonical,
       SUBTYPE_DIFF = daterange_subdiff,
       SUBTYPE_OPCLASS = date_ops
);

CREATE TYPE cint4range;

CREATE FUNCTION cint4range_canonical(cint4range) 
RETURNS cint4range
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT IMMUTABLE;

CREATE TYPE cint4range as RANGE (
       SUBTYPE = integer,
       CANONICAL = cint4range_canonical,
       SUBTYPE_DIFF = int4range_subdiff,
       SUBTYPE_OPCLASS = int4_ops
);

CREATE TYPE cint8range;

CREATE FUNCTION cint8range_canonical(cint8range) 
RETURNS cint8range
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT IMMUTABLE;

CREATE TYPE cint8range as RANGE (
       SUBTYPE = bigint,
       CANONICAL = cint8range_canonical,
       SUBTYPE_DIFF = int8range_subdiff,
       SUBTYPE_OPCLASS = int8_ops
);



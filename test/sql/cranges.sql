

-- use closed form
select cdaterange('2015-06-11','2015-06-20','[]');

-- two argument form defaults to '[)'
-- but result still comes out as closed form
select cdaterange('2015-06-11','2015-06-20');

select cint4range(1,5,'[]');

select cint4range(1,5);

select cint8range(1::bigint,5::bigint,'[]');

select cint8range(1::bigint,5::bigint);

CREATE TABLE mytable (
    t    text,
    r    cdaterange
);

INSERT INTO mytable
VALUES ('stuff', '[2015-06-11, 2015-06-29]');

SELECT * FROM mytable;

-- change to use native type instead
alter table mytable 
    alter column r 
          type daterange 
          using daterange(lower(r), upper(r), '[]');

SELECT * FROM mytable;



